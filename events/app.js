const EventEmmiter = require('events');


const myEmmiter = new EventEmmiter();

const logDbConnection = () => {
    console.log('DB connected');
};

myEmmiter.addListener('connected', logDbConnection);
myEmmiter.emit('connected');

myEmmiter.removeListener('connected', logDbConnection);
// myEmmiter.removeAllListeners('connected');
myEmmiter.emit('connected');
myEmmiter.on('msg', (data) => console.log(`Get message: ${data}`))

myEmmiter.prependListener('msg', (data) => {
    console.log('Prepend');
})


myEmmiter.emit('msg', 'Hello, get my message')

myEmmiter.once('off', () => {
    console.log('message get 1');
});

myEmmiter.emit('off');
myEmmiter.emit('off');

console.log(myEmmiter.getMaxListeners());
myEmmiter.setMaxListeners(1);
console.log(myEmmiter.getMaxListeners());
console.log(myEmmiter.listenerCount('msg'));
console.log(myEmmiter.listenerCount('off'));

console.log(myEmmiter.eventNames());

myEmmiter.on('error', (error) => {
    console.log(error.message);
})

myEmmiter.emit('error', new Error('Yopta'))


